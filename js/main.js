$(function () {
    // Enable everywhere
    $('[data-toggle="tooltip"]').tooltip();
    $('.x-scrollbar').perfectScrollbar();
    $('.x-animateOnscroll').viewportChecker({
        classToAdd: 'active',
        repeat: true
    });


    if (document.getElementById("map")) {
        new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 8
        });
    }


    if (document.getElementById("range-slider")) {
        $('#range-slider').jRange({
            from: 0,
            to: 100,
            step: 1,
            isRange: true,
            showScale: false,
        });
    }



    if (document.getElementById("DataTable")) {
        $('#DataTable tfoot th').each( function () {
            let title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );

        // DataTable
        let table = $('#DataTable').DataTable({
            searching: false,

            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false
        });

        // Apply the search
        table.columns().every( function () {
            let that = this;

            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    }


    // social-share.html
    if (document.getElementById("socialShare1")) {
        let bigCharCtx = document.getElementById("socialShare1").getContext("2d");
        new Chart(bigCharCtx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#002EAE", '#E0E0E0'
                        ]
                    },
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#0059FF", '#E0E0E0'
                        ]
                    },
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#00D37E", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });


        setTimeout(function(){
            let smallChar1ctx = document.getElementById("socialShare2").getContext("2d");
            new Chart(smallChar1ctx, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#002EAE", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#0059FF", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#00D37E", '#E0E0E0'
                            ]
                        }
                    ],
                    labels: [
                        ""
                    ]
                },
                options: {
                    cutoutPercentage: 70,
                    radiusBackground: {color: '#E0E0E0'},
                    legend: {display: false},
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }
            });
        }, 350);

        setTimeout(function(){
            let smallChar2ctx = document.getElementById("socialShare3").getContext("2d");
            new Chart(smallChar2ctx, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#002EAE", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#0059FF", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#00D37E", '#E0E0E0'
                            ]
                        }
                    ],
                    labels: [
                        ""
                    ]
                },
                options: {
                    cutoutPercentage: 70,
                    radiusBackground: {color: '#E0E0E0'},
                    legend: {display: false},
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }
            });
        }, 700);

        setTimeout(function(){
            let smallChar3ctx = document.getElementById("socialShare4").getContext("2d");
            new Chart(smallChar3ctx, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#002EAE", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#0059FF", '#E0E0E0'
                            ]
                        },
                        {
                            data: [
                                90, 10
                            ],
                            backgroundColor: [
                                "#00D37E", '#E0E0E0'
                            ]
                        }
                    ],
                    labels: [
                        ""
                    ]
                },
                options: {
                    cutoutPercentage: 70,
                    radiusBackground: {color: '#E0E0E0'},
                    legend: {display: false},
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }
            });
        }, 1050);
    }

    // social-share-a.html
    if (document.getElementById("socialShareA1")) {
        let bigCharCtx = document.getElementById("socialShareA1").getContext("2d");
        new Chart(bigCharCtx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#002EAE", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar1ctx = document.getElementById("socialShareA2").getContext("2d");
        new Chart(smallChar1ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#002EAE", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar2ctx = document.getElementById("socialShareA3").getContext("2d");
        new Chart(smallChar2ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#002EAE", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar3ctx = document.getElementById("socialShareA4").getContext("2d");
        new Chart(smallChar3ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#002EAE", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });
    }

    // social-share-b.html
    if (document.getElementById("socialShareB1")) {
        let bigCharCtx = document.getElementById("socialShareB1").getContext("2d");
        new Chart(bigCharCtx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#0043FF", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar1ctx = document.getElementById("socialShareB2").getContext("2d");
        new Chart(smallChar1ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#0043FF", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar2ctx = document.getElementById("socialShareB3").getContext("2d");
        new Chart(smallChar2ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#0043FF", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar3ctx = document.getElementById("socialShareB4").getContext("2d");
        new Chart(smallChar3ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#0043FF", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });
    }

    // social-share-c.html
    if (document.getElementById("socialShareC1")) {
        let bigCharCtx = document.getElementById("socialShareC1").getContext("2d");
        new Chart(bigCharCtx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#00D37E", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar1ctx = document.getElementById("socialShareC2").getContext("2d");
        new Chart(smallChar1ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#00D37E", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar2ctx = document.getElementById("socialShareC3").getContext("2d");
        new Chart(smallChar2ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#00D37E", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });



        let smallChar3ctx = document.getElementById("socialShareC4").getContext("2d");
        new Chart(smallChar3ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [
                            90, 10
                        ],
                        backgroundColor: [
                            "#00D37E", '#E0E0E0'
                        ]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: {
                cutoutPercentage: 70,
                radiusBackground: {color: '#E0E0E0'},
                legend: {display: false},
                elements: {
                    arc: {
                        borderWidth: 0
                    }
                }
            }
        });
    }





    let circleOptions = {
        cutoutPercentage: 0,
        radiusBackground: {color: '#E0E0E0'},
        legend: {display: false},
        tooltips: {enabled: false},
        elements: {
            arc: {
                borderWidth: 1,
                borderColor: 'rgba(101, 225, 255, 0.25)'
            }
        }
    };
    if (document.getElementById("circle")) {
        let circleCtx = document.getElementById("circle").getContext("2d");
        new Chart(circleCtx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 0, 0, 0)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 67, 255, 0.5)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: circleOptions
        });
    }

    if (document.getElementById("line")) {
        let lineCtx = document.getElementById("line").getContext("2d");
        new Chart(lineCtx, {
            type: 'line',
            data: {
                labels: [12, 19, 3, 5, 2, 3, 1, 19, 3, 5, 2, 3, 19, 3, 5, 2, 3, 55],
                datasets: [
                    {
                        data: [12, 19, 3, 5, 2, 3, 1, 19, 3, 5, 2, 3, 19, 3, 5, 2, 3, 23],
                        borderWidth: 1
                    },
                    {
                        data: [7, 11, 5, 8, 3, 7, 19, 3, 5, 5, 8, 3, 7, 19, 3, 5, 2, 3],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                elements: { point: { radius: 0 } },
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            display: false
                        }
                    }]
                }
            }
        });
    }

    if (document.getElementById("circle2")) {
        let circle2Ctx = document.getElementById("circle2").getContext("2d");
        new Chart(circle2Ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [1],
                        backgroundColor: ["rgba(0, 67, 255, 0.5)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#0043FF"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["rgba(255, 57, 202, 0.75)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: circleOptions
        });
    }

    if (document.getElementById("circle3")) {
        let circle3Ctx = document.getElementById("circle3").getContext("2d");
        new Chart(circle3Ctx, {
            type: 'doughnut',
            data: {
                datasets: [
                    {
                        data: [1],
                        backgroundColor: ["rgba(255, 57, 202, 0.75)"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    },
                    {
                        data: [1],
                        backgroundColor: ["#FF39CA"]
                    }
                ],
                labels: [
                    ""
                ]
            },
            options: circleOptions
        });
    }




    if (document.getElementById("lineChar")) {
        let lineCtx = document.getElementById("lineChar").getContext("2d");

        let data = [4, 5, 7, 5, 4, 8, 5, 4];
        let labels = ['71 / 2033', '71 / 2033', '71 / 2033', '71 / 2033', '71 / 2033', '71 / 2033', '71 / 2033', '71 / 2033'];
        let max = Math.max.apply(Math, data);

        let options = {
            animation: {
                onComplete: function() {
                    let ctx = this.chart.ctx;
                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";
                    let chart = this;
                    let datasets = this.config.data.datasets;

                    datasets.forEach(function(dataset, i) {
                        ctx.font = "24px Roboto Two";
                        ctx.fillStyle = "#002EAE";
                        chart.getDatasetMeta(i).data.forEach(function(p, j) {
                            ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 30);
                        });
                    });
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            events: false,
            scales: {
                yAxes: [{
                    ticks: {
                        max: max + 2,
                        display: false,
                        beginAtZero: true
                    },

                    gridLines:{
                        display : false,
                        drawBorder: false
                    }
                }],
                xAxes: [{
                    position: 'top',

                    gridLines:{
                        borderDash: [3, 4],
                        color: "#BBBBBB",
                        drawBorder: false
                    }
                }]
            },
            elements: {
                line: {
                    fill: false
                },
                point: { radius: 5 }
            }
        };



        new Chart(lineCtx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    fill: true,
                    backgroundColor: "#F4F4F4",
                    lineTension: 0,
                    data: data,
                    borderColor: '#8B8B8B',
                    pointBackgroundColor: "#002EAE"
                }]
            },
            options: options
        });
    }
    if (document.getElementById("barsChar")) {
        let img = new Image();
        img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALMAAAByCAYAAAAYoODiAAAABHNCSVQICAgIfAhkiAAAAqhJREFUeJzt1j1ym2AARVEQ6CercpX97yGrsCQk0kQexnaSKhn7+rwOEKPvVNxhMItsfHp6+v64WJZlmOf55eH1ej3v9/vj9oXr9Xrb7/fT4/p8Pq/H43H89f7zPM+n+/1+3+12u2VZnodhGOZ5Pj2e3W63dZqm8Xf/cblcLofD4bA50/M8z6dXZ3hzrtvtdp2mab+9x/N1POM4/pjXdR03767b63Vdx1fP37v38s7j2fY3r+9N0zRs35+mafzTf753hsf9v5yL5wt5xvHNEcw+72QGT8IjM3gyHplhqckMnoRHZvBkPDLDUpMZPAmPzODJeGSGpSYzeBIemcGT8cgMS01m8CQ8MoMn45EZlprM4El4ZAZPxiMzLDWZwZPwyAyejEdmWGoygyfhkRk8GY/MsNRkBk/CIzN4Mh6ZYanJDJ6ER2bwZDwyw1KTGTwJj8zgyXhkhqUmM3gSHpnBk/HIDEtNZvAkPDKDJ+ORGZaazOBJeGQGT8YjMyw1mcGT8MgMnoxHZlhqMoMn4ZEZPBmPzLDUZAZPwiMzeDIemWGpyQyehEdm8GQ8MsNSkxk8CY/M4Ml4ZIalJjN4Eh6ZwZPxyAxLTWbwJDwygyfjkRmWmszgSXhkBk/GIzMsNZnBk/DIDJ6MR2ZYajKDJ+GRGTwZj8yw1GQGT8IjM3gyHplhqckMnoRHZvBkPDLDUpMZPAmPzODJeGSGpSYzeBIemcGT8cgMS01m8CQ8MoMn45EZlprM4El4ZAZPxiMzLDWZwZPwyAyejEdmWGoygyfhkRk8GY/MsNRkBk/CIzN4Mh6ZYanJDJ6ER2bwZDwyw1KTGTwJj8zgyXhkhqUmM3gSHpnBk/HIDEvtn2fGsizr6XT69tk/Yzwf2/NfMuN+v+8KnzGej+0Zx3H4Caz7jdcslcvOAAAAAElFTkSuQmCC';


        let data1 = [65, 59, 80, 81, 56, 55, 40, 55];
        let data2 = [28, 48, 40, 19, 86, 27, 90, 35];
        let labels = ["12/02/17", "12/02/17", "12/02/17", "12/02/17", "12/02/17", "12/02/17", "12/02/17", "12/02/17"];
        let max = Math.max.apply(Math, data1.concat(data2));


        img.onload = function() {
            let barsChar = document.getElementById("barsChar").getContext("2d");
            let fillPattern=barsChar.createPattern(img, 'repeat');

            new Chart(barsChar, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        backgroundColor: fillPattern,
                        data: data1
                    }, {
                        backgroundColor: "rgba(62, 62, 62, 0.4)",
                        data: data2
                    }]
                },
                options: {
                    animation: {
                        onComplete: function() {
                            let ctx = this.chart.ctx;
                            ctx.textAlign = "center";
                            ctx.textBaseline = "middle";
                            let chart = this;
                            let datasets = this.config.data.datasets;

                            datasets.forEach(function(dataset, i) {
                                ctx.font = "12px Roboto Two";
                                ctx.fillStyle = "#4F4C4D";

                                let leftMargin = -12;
                                let leftInfo = 'm';
                                if (i % 2 == 0) {
                                    leftMargin = 12;
                                    leftInfo = 'km';
                                }

                                chart.getDatasetMeta(i).data.forEach(function(p, j) {
                                    ctx.fillText(datasets[i].data[j] + leftInfo, p._model.x - leftMargin, p._model.y - 12);
                                });
                            });
                        }
                    },
                    tooltips: {
                        enabled: false
                    },
                    events: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {display: false},
                    scales: {
                        yAxes: [{
                            ticks: {
                                max: max + 8,
                                display: false,
                            },

                            gridLines: {
                                display: false,
                                drawBorder: false
                            }
                        }],
                        xAxes: [{
                            barPercentage: 1,
                            barThickness: 47,
                            categoryPercentage: 0.5,

                            gridLines: {
                                display: false,
                                drawBorder: false
                            }
                        }]
                    }
                }
            });
        };
    }



    if ($(".x-racing").length) {
        let maxHeight = $(".x-racing").height();
        let minHeight = 80;
        let racingArr = [
            {color: '', name: 'Salim Harfenata', progress: 0},
            {color: '', name: 'Jeanine Dupont', progress: 3},
            {color: '', name: 'Jean-François Demeyre', progress: 14.5},
            {color: '', name: 'Aimé Yeurveu', progress: 22.5},
            {color: 'blue', name: 'Florian Laroue', progress: 25.5},
            {color: 'blue', name: 'Romain Gaspard', progress: 38},
            {color: '', name: 'Aurélien Barranger', progress: 41},
            {color: 'blue', name: 'Alejandro Martinez', progress: 56.6},
            {color: 'pink', name: 'Jérôme Legenne', progress: 66},
            {color: '', name: 'Simon Tallasso', progress: 71},
            {color: '', name: 'Sylvie Lafleche', progress: 80},
            {color: 'blue', name: 'Steve Gallette', progress: 83},
            {color: '', name: 'Olivier Megawatt', progress: 87.2},
            {color: '', name: 'Alexandre Guiral', progress: 91}
        ];

        for (let i = 0; i < racingArr.length; i++) {
            let item = racingArr[i];
            let flag = '';
            let rndInteger = Math.floor(Math.random() * (maxHeight - minHeight + 1) ) + minHeight;

            // if (item.progress > 85) {
            //     flag = 'left';
            // }

            $( ".x-racing" ).append('<div class="x-racing-item ${item.color} ${flag}" \n' +
                '                  style="left: ${item.progress}%;height: ${rndInteger}px;">\n' +
                '                    <i class="x-racing-item-icon x-icon-bicycle-a"></i>\n' +
                '                    <div class="x-racing-item-name">${item.name}</div>\n' +
                '                </div>');
        }
    }

});